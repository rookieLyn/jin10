// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

DWORD reg_EAX = 0;
DWORD reg_EDI = 0;
DWORD reg_EBP = 0;
HMODULE hm = 0;
DWORD offsetE = 0xE8C95;
DWORD offsetC = 0xbbe7E;// 0xf6744;//44;
void SetWindowEchartLog();
void SetCursorLog();
char logbuf[100] = { 0 };
DWORD jmpbackE = 0;
DWORD jmpbackC = 0;
_declspec(naked) void getEaxEsi(){
	__asm{
		mov reg_EAX, eax;
		mov reg_EDI, edi;
	}
	if (0 == strcmp((char*)reg_EAX, (char*)reg_EDI)){
		sprintf(logbuf, "window : %s, packet : %s\n", (char*)reg_EAX, (char*)reg_EDI);
		printf(logbuf);
	}
	__asm{
		mov ecx, reg_EAX
		or ebx,1
		jmp jmpbackE
	}
//	_asm ret
}
_declspec(naked) void getEdx(){
	__asm{
		mov reg_EBP, ebp;
		pushad;
	/*	push edi;
		push edx;
		push ecx;
		push eax;*/
		movss xmm0, [ebp - 0x68];
		cvtps2pd xmm0, xmm0;
		push ecx;
		push ecx;
		movsd [esp], xmm0;
	}
	
	sprintf(logbuf, "update on : %s , data is : %g\n", (char*)reg_EBP -0x28/*,*(DWORD*)((char*)reg_EBP - 0x68)*/);
	printf(logbuf);
	
	__asm{
	/*	pop eax;
		pop ecx;
		pop edx;
		pop edi;*/
		pop ecx;
		pop ecx;
		popad;

		push edi;
		lea eax, [ebp - 0x78];
		push eax;
		lea eax, [ebp - 0x28];
		jmp jmpbackC;
	}
//	_asm ret
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
					   LPVOID lpReserved
					 )
{
	char buf[100] = { 0 };

	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	//	getEaxEsi();
	//	sprintf(buf, "EAX is %08x , ESI is%08x\n", reg_EAX, reg_EDI);
//		MessageBoxA(NULL,"666","pause",MB_OK);
		//get the Address of window charts
		
		hm = GetModuleHandle(NULL);
	//	SetWindowEchartLog();
		SetCursorLog();
	//	SetWindowEchartLog();
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
//#ifdef _DEBUG		
	AllocConsole();						//打开控制台窗口以显示调试信息
//	AttachConsole(GetCurrentProcessId());
	SetConsoleTitleA("Debug Win Output");			//设置标题
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);		//获取控制台输出句柄
	INT hCrt = _open_osfhandle((INT)hCon, _O_TEXT);		//转化为C文件描述符
	FILE * hf = _fdopen(hCrt, "w");			//转化为C文件流
	setvbuf(hf, NULL, _IONBF, 0);				//无缓冲
	*stdout = *hf;						//重定向标准输出
//#endif
	return TRUE;
}


void SetWindowEchartLog(){
	DWORD protect = 0;
	jmpbackE = (DWORD)hm + offsetE + 5;
	VirtualProtect((char*)hm + offsetE, 5, PAGE_EXECUTE_READWRITE,&protect);
	char cmd[20] = {0};
	char old[20] = { 0 };//mov ecx,eax;or ebx,1; 5 bytes
	cmd[0] = 0xe9;//jmp
	DWORD dis = (DWORD)getEaxEsi - ((DWORD)hm + offsetE) - 5;
	*(DWORD*)&cmd[1] = dis;
	
	memcpy(old, (char*)((DWORD)hm + offsetE), 5);
	memcpy((char*)((DWORD)hm + offsetE), cmd, 5);
	if (!VirtualProtect((char*)hm + offsetE, 5, protect, &protect)){
		MessageBox(NULL, L"failed000000", L"quiet", MB_OK);
	}
}
void SetCursorLog(){
	DWORD protect = 0;
	jmpbackC = (DWORD)hm + offsetC + 8;
	bool b = VirtualProtect((char*)hm + offsetC, 6, PAGE_EXECUTE_READWRITE, &protect);
	if (b){
		char cmd[20] = { 0 };
		char old[20] = { 0 };// 6 bytes
		cmd[0] = 0xe9;//jmp
		DWORD dis = (DWORD)getEdx - ((DWORD)hm + offsetC) - 5;
		*(DWORD*)&cmd[1] = dis;

		memcpy(old, (char*)((DWORD)hm + offsetC), 6);
		memcpy((char*)((DWORD)hm + offsetC), cmd, 5);
	//	//	memcpy((char*)((DWORD)hm + offsetC), old, 6);
		if (!VirtualProtect((char*)hm + offsetC, 6, protect, &protect)){
			MessageBox(NULL, L"failed11111111", L"quiet", MB_OK);
		}
	}
	else
	{
		MessageBox(NULL,L"failed",L"quiet",MB_OK);
	}
}




