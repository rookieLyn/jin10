
// blockKlineDlg.cpp : implementation file
//

#include "stdafx.h"
#include "blockKline.h"
#include "blockKlineDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CblockKlineDlg dialog



CblockKlineDlg::CblockKlineDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CblockKlineDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CblockKlineDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CblockKlineDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(ID_BTNBLOCK, &CblockKlineDlg::OnBnClickedBtnblock)
END_MESSAGE_MAP()


// CblockKlineDlg message handlers

BOOL CblockKlineDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CblockKlineDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CblockKlineDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CblockKlineDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

#include <Windows.h>
#include<TlHelp32.h>
using namespace std;
int GetProcessIDByName(LPCTSTR name)
{
	STARTUPINFO st; PROCESS_INFORMATION pi;
	PROCESSENTRY32 ps; HANDLE hSnapshot;

	ZeroMemory(&st, sizeof(STARTUPINFO));
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	st.cb = sizeof(STARTUPINFO);
	ZeroMemory(&ps, sizeof(PROCESSENTRY32));
	ps.dwSize = sizeof(PROCESSENTRY32);
	hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);//拍摄进程快照 
	if (hSnapshot == INVALID_HANDLE_VALUE)//快照拍摄失败 
	{
		return 0;
	}
	if (!Process32First(hSnapshot, &ps))
	{
		return 0;
	}

	do
	{
		if (lstrcmpi(ps.szExeFile, name) == 0)//遍历进程快照，比较进程名 
		{
			return ps.th32ProcessID;//将进程返回
		}
	} while (Process32Next(hSnapshot, &ps)); // 没有找到  CloseHandle(hSnapshot); return dwPID;//返回容器

}
BYTE* GetProcessBase(int pid, LPCTSTR modname){
	HANDLE hm = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);
	int d = GetLastError();
	if (0 == hm){
		return 0;
	}
	MODULEENTRY32 ms = { 0 };
	ms.dwSize = sizeof(MODULEENTRY32);
	while (false == Module32First(hm, &ms)){
		d = GetLastError();
		if (0x18 != d)
			break;
	}
	do
	{
		if (0 == wcscmp(modname, ms.szModule)){
			return ms.modBaseAddr;
		}
	} while (Module32Next(hm, &ms));
	return NULL;
}

CHAR btnText[100] = { 0 };
CHAR *newTEXT = (CHAR*)__TEXT("Success!");
BOOL SetPrivilege(HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege);

void CblockKlineDlg::OnBnClickedBtnblock()
{
	// TODO: Add your control notification handler code here
	HANDLE hToken;
	BOOL bRet = OpenProcessToken(
		GetCurrentProcess(),    // 进程句柄(当前进程)
		TOKEN_ALL_ACCESS,    // 全权访问令牌
		&hToken    // 返回的参数 进程令牌句柄 （就是AdjustTokenPrivileges的第一个参数）
		); // 获取进程的令牌句柄
	if (bRet != TRUE)   {
		MessageBox(L"获取令牌句柄失败", L"in", MB_OK);
		return;
	}
	BOOL set = SetPrivilege(hToken, SE_DEBUG_NAME, TRUE);
	if (!set || GetLastError() != ERROR_SUCCESS)  {
		MessageBox(L"提升权限失败",L"in",MB_OK);
		return;
	}

	char dllName[20] = {0};
	strcpy_s(dllName,"patch.dll");
	int offset = 0xE8C95;//window packet
	int pid = GetProcessIDByName(L"TraderMaster.exe");
	if (0 == pid){
		return;
	}
	HANDLE h = OpenProcess(PROCESS_ALL_ACCESS, NULL, pid);
	if (NULL == h){
		return;
	}
	BYTE* base = GetProcessBase(pid, L"TraderMaster.exe");
	BYTE* kerBase = GetProcessBase(pid,L"kernel32.dll");
	if (NULL == base){
		return;
	}
	LPVOID lpMemoryAddress = VirtualAllocEx(h, NULL,
		sizeof(dllName)+ 4, MEM_COMMIT, PAGE_READWRITE);
	int d = GetLastError();
	//LoadProcessAddress
	HMODULE hm = LoadLibrary(L"kernel32.dll");
	FARPROC ldl = GetProcAddress(hm, "LoadLibraryA");
	DWORD dis_ldl = (DWORD)ldl - (DWORD)hm;
	DWORD real_adr_Target_ldl = (DWORD)kerBase + dis_ldl;

	DWORD writed = 0;
	if (0 == WriteProcessMemory(h, lpMemoryAddress, dllName, sizeof(dllName), &writed))
	{
		return;
	}
	DWORD ThreadId = 0;
	HANDLE hThread = CreateRemoteThread(h, NULL, 0, (LPTHREAD_START_ROUTINE)real_adr_Target_ldl,
		lpMemoryAddress, 0, &ThreadId);
	//wait fot return
	WaitForSingleObject(hThread, INFINITE);//==WAIT_OBJECT_0)
	VirtualFreeEx(h, lpMemoryAddress, 0, MEM_RELEASE);//release the remote address;
	//
	/*char buf[12] = { 0 };
	char cmd[] = { 0xeb, 0xe };
	SIZE_T bytesread = 0;
	if (0 == ReadProcessMemory(h, base + offset, buf, 2, &bytesread)){
		return;
	}
	if (0 == WriteProcessMemory(h, base + offset, cmd, 2, &bytesread)){
		return;
	}

	GetDlgItemText(ID_BTNBLOCK, (LPTSTR)btnText, 100);
	SetDlgItemText(ID_BTNBLOCK, (LPTSTR)newTEXT);*/
}
//int* EAX = 0;
//int* ESI = 0;
DWORD reg_EAX = NULL;
DWORD reg_ESI = NULL;
void  __declspec(naked) getValue(){
	__asm{
		mov reg_EAX, eax;
		mov reg_ESI, esi;
		ret;
	}
}
BOOL SetPrivilege(HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege)
{
	TOKEN_PRIVILEGES tp; // 该结构包含一个数组，数据组的每个项指明了权限的类型和要进行的操作
	LUID luid;
//	getValue();

	// 查找
	if (!LookupPrivilegeValue(
		NULL,            // 系统的名字,null,在本地系统上查找权限 lookup privilege on local system
		lpszPrivilege,   // 要找的权限名 privilege to lookup 
		&luid))        // 通过指针返回权限的LUID receives LUID of privilege
	{
		printf("LookupPrivilegeValue error: %u\n", GetLastError());
		return FALSE;
	}

	tp.PrivilegeCount = 1;    // 要修改的特权数目
	tp.Privileges[0].Luid = luid;    // 代表不同特权类型
	if (bEnablePrivilege)
		tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
	else
		tp.Privileges[0].Attributes = 0;

	// 调整访问令牌的权限 Enable the privilege or disable all privileges.
	if (!AdjustTokenPrivileges(
		hToken,// OpenProcessToken第三个指针参数传出的访问令牌的句柄
		FALSE, // 是否禁用所有所有的特权
		&tp,    // 指明要修改的权限
		sizeof(TOKEN_PRIVILEGES),    // PreviousState的长度
		NULL,   // 存放修改前的访问权限的信息，可空
		NULL))    // 实际PreviousState结构返回的大小
	{
		printf("AdjustTokenPrivileges error: %u\n", GetLastError());
		return FALSE;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)
	{
		printf("令牌在NewState参数中没有这个或这些权限 \n");
		return FALSE;
	}

	return TRUE;
}